import React, {useState} from 'react';
import Card from "../card/Card";
import "./Body.scss"
import Button from "../button/Button";
import CustomBtn from "../customBtn/customBtn";
import Modal from "../modal/Modal";

function Body(props) {
    const {goods, goodsInCart, goodsInFavourite, updateDataInCart, updateDataInFavourite} = props;

    const [showModal, setShowModal] = useState(false);

    const goodItems = goods.map(good => (
        <div key={good.vendorCode}>
            <Card good={good}
                  children={
                      <>
                          <Button onClick={() => handleClick(good)}>
                              {goodsInCart.find(item => item.vendorCode === good.vendorCode) ? "Already in cart" : "Add to cart"}
                          </Button>
                          <CustomBtn
                              elemClasses={
                                  `favourite
                                  ${goodsInFavourite.find(item => item.vendorCode === good.vendorCode)
                                      ? "favourite_is_checked" : ""}`
                              }
                              onClick={() => addToFavourite(good)}
                          />
                      </>
                  }
            />

        </div>
    ))

    const addToCart = (good) => {
        const checkData = good.vendorCode;

        if (!goodsInCart.find(item => item.vendorCode === checkData)) {
            updateDataInCart([...goodsInCart, good]);
        }
    }

    const addToFavourite = (good) => {
        const checkData = good.vendorCode;

        if (!goodsInFavourite.find(item => item.vendorCode === checkData)) {
            updateDataInFavourite([...goodsInFavourite, good]);

        } else {
            const newData = goodsInFavourite.filter((element) => {
                return element.vendorCode !== good.vendorCode;
            })
            updateDataInFavourite(newData);
        }
    }

    const showWindow = () => {
        setShowModal(true);
        document.body.classList.add("modal-open");
    }

    const handleClick = (good) => {
        addToCart(good);
        showWindow();
    }

    const closeWindow = () => {
        setShowModal(false);
        document.body.classList.remove("modal-open");
    }

    return (
        <div className="main">
            {goodItems}
            {showModal &&
            <Modal
                onClick={closeWindow}
                info="Товар упешно добавлен в корзину!"
            >
                <Button onClick={closeWindow}>OK</Button>
            </Modal>}
        </div>
    );
}

export default Body;