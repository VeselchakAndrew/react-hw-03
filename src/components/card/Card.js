import React from 'react';
import PropTypes from "prop-types";
import "./Card.scss";

function Card(props) {
    const {name, price, color, img} = props.good;
    const {children} = props;
    return (
        <div className="card">
            <div className="card_image_box"><img src={img} alt="Car" className="card_image"></img></div>
            <div>Название: <span className="card_info">{name}</span></div>
            <div>Цена: <span className="card_info">{price}</span></div>
            <div>Цвет: <span className="card_info">{color}</span></div>
            {children}
        </div>
    );
}

Card.propType = {
    vendorCode: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    color: PropTypes.string,
    img: PropTypes.string
}

Card.defaultProps = {
    color: "Уточняйте",
    img: "img/default.img"
}


export default Card;
