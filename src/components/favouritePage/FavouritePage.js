import React, {useState} from "react";
import Card from "../card/Card";
import CustomBtn from "../customBtn/customBtn";
import Modal from "../modal/Modal";
import Button from "../button/Button";

function FavouritePage(props) {
    const {goodsInFavourite, goodsInCart, updateDataInFavourite, updateDataInCart} = props;
    const [showModal, setShowModal] = useState(false);

    const addToCart = (good) => {
        const checkData = good.vendorCode;

        if (!goodsInCart.find(item => item.vendorCode === checkData)) {
            updateDataInCart([...goodsInCart, good]);
        }
    }

    const removeFromFavourite = (good) => {

        const newData = goodsInFavourite.filter((element) => {
            return element.vendorCode !== good.vendorCode;
        })
        updateDataInFavourite(newData);

    }

    const showWindow = () => {
        setShowModal(true);
        document.body.classList.add("modal-open");
    }

    const closeWindow = () => {
        setShowModal(false);
        document.body.classList.remove("modal-open");
    }

    const handleClick = (good) => {
        addToCart(good);
        showWindow();
    }

    const cardItem = goodsInFavourite.map((good) => (
        <div key={good.vendorCode}>
            <Card good={good} children={
                <>
                    <Button onClick={() => handleClick(good)}>
                        {goodsInCart.find(item => item.vendorCode === good.vendorCode) ? "Already in cart" : "Add to cart"}
                    </Button>
                    <CustomBtn
                        elemClasses={
                            `favourite ${goodsInFavourite.find(item => item.vendorCode === good.vendorCode)
                                ? "favourite_is_checked" : ""}`
                        }
                        onClick={() => removeFromFavourite(good)}
                    />
                </>}
            />
        </div>
    ));

    return (
        <div className="main">
            {cardItem}
            {showModal &&
            <Modal
                onClick={closeWindow}
                info="Товар упешно добавлен в корзину!"
            >
                <Button onClick={closeWindow}>OK</Button>
            </Modal>}
        </div>
    )
}

export default FavouritePage;