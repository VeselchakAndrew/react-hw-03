import React, {useState} from "react";
import Card from "../card/Card";
import CustomBtn from "../customBtn/customBtn";
import Modal from "../modal/Modal";
import "./Cart.scss";
import Button from "../button/Button";

function Cart(props) {
    const {goodsInCart, updateDataInCart} = props;

    const [modalWindowState, setModalWindowState] = useState(false);
    const [goodToRemove, setGoodToRemove] = useState(0);

    const showModalWindow = (id) => {
        setModalWindowState(true);
        document.body.classList.add("modal-open");
        setGoodToRemove(id);
    }


    const closeModalWindow = () => {
        setModalWindowState(false);
        document.body.classList.remove("modal-open");
        setGoodToRemove(0);
    };

    const removeFromCart = (good) => {
        console.log(good)
        const newData = goodsInCart.filter((id) => {
            return id.vendorCode !== good;
        })

        updateDataInCart(newData);
        closeModalWindow();
    }

    const cardItem = goodsInCart.map((good) => (
        <div key={good.vendorCode}>
            <Card good={good} children={
                <CustomBtn
                    elemClasses="delete_btn"
                    onClick={showModalWindow.bind(null, good.vendorCode)}
                />}
            />
        </div>
    ));

    return (
        <div className="main">
            {cardItem}
            {modalWindowState &&
            <Modal onClick={closeModalWindow} info="Вы уверены?">
                <>
                    <Button onClick={() => removeFromCart(goodToRemove)}>Удалить</Button>
                    <Button onClick={closeModalWindow}>Отмена</Button>
                </>
            </Modal>
            }
        </div>
    )
}

export default Cart;