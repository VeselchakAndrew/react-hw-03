import React from 'react';
import Navigation from "../navigation/Navigation"
import "./Header.scss"

function Header(pros) {

    return (
        <div className="header">
            <h1 className="header_title">Hot Wheels</h1>
            <ul className="Navigation">
                <Navigation name="Main page" to="/"/>
                <Navigation name="Favourite" to="/favourite"/>
                <Navigation name="Cart" to="/cart"/>
            </ul>
        </div>
    );
}

export default Header;