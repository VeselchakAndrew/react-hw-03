import React from 'react';
import PropTypes from "prop-types";
import "./Button.scss"

function Button(props) {

    const {children, onClick} = props;
    return (
        <button className="button" onClick={onClick}>{children}</button>
    );
}

Button.propType = {
    children: PropTypes.string,
    onClick: PropTypes.func.isRequired

}

Button.defaultTypes = {
    children: "OK"
}

export default Button;