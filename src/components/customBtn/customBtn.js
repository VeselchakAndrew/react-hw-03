import React from 'react';

function CustomBtn(props) {
    const {onClick, elemClasses} = props;
    return (
        <div className={elemClasses} onClick={onClick}></div>
    );
}

export default CustomBtn;