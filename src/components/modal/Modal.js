import React from 'react';
import PropTypes from "prop-types";
import "./Modal.scss"

function Modal(props) {

    const preventCloseModal = (e) => {
        e.stopPropagation();
    }


    const {onClick, info, children} = props;
    return (
        <div onClick={onClick} className="ModalBg">
            <div className="Modal" onClick={preventCloseModal}>
                <div className="Header">
                    <div className="Cross" onClick={onClick}></div>
                </div>
                <div className="Main_content">
                    <div>{info}</div>
                    {children}
                </div>
            </div>

        </div>
    )
}

Modal.propType = {
    onClick: PropTypes.func.isRequired,
}


export default Modal;