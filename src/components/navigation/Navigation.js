import React from "react";
import "./Navigation.scss"
import {Link} from "react-router-dom";

function Navigation(props) {
    const {name, to, component} = props;
    return (
        <Link className="Navigation_element" to={to} component={component}>{name}</Link>
    )
}

export default Navigation;