import React, {useState, useEffect} from 'react';
import './App.css';
import Header from "./components/header/Header";
import Body from "./components/body/Body";
import Cart from "./components/cart/Cart";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import FavouritePage from "./components/favouritePage/FavouritePage";

function App() {
    const [goods, setGoods] = useState([]);
    const [goodsInCart, setGoodsInCart] = useState([]);
    const [goodsInFavourite, setGoodsInFavourite] = useState([]);

    const dataBase = `baza.json`;
    const getData = async () => {
        const goodsDataResponse = await fetch(dataBase);
        const goodsData = await goodsDataResponse.json();
        setGoods(goodsData);
    }

    useEffect(() => {
        getData();
        checkLocalStorageState("Favourite", setGoodsInFavourite);
        checkLocalStorageState("Cart", setGoodsInCart);
    }, [])

    useEffect(() => {
        localStorage.setItem("Cart", JSON.stringify(goodsInCart));
        localStorage.setItem("Favourite", JSON.stringify(goodsInFavourite));
    })


    const checkLocalStorageState = (key, state) => {
        if (!localStorage.getItem(key)) {
            (localStorage.setItem(`${key}`, JSON.stringify([])))
        } else {
            state(JSON.parse(localStorage.getItem(key)))
        }
    }

    const updateDataInCart = (value) => {
        setGoodsInCart(value);
        localStorage.setItem("Cart", JSON.stringify(setGoodsInCart));
    }

    const updateDataInFavourite = (value) => {
        setGoodsInFavourite(value);
        localStorage.setItem("Favourite", JSON.stringify(setGoodsInFavourite));
    }

    return (
        <Router>
            <div className="App">
                <Header/>
                <Switch>
                    <Route path="/" exact>
                        <Body goods={goods}
                              goodsInFavourite={goodsInFavourite}
                              goodsInCart={goodsInCart}
                              updateDataInCart={updateDataInCart}
                              updateDataInFavourite={updateDataInFavourite}/>
                    </Route>
                    <Route path="/cart"><Cart goodsInCart={goodsInCart} updateDataInCart={updateDataInCart}/></Route>
                    <Route path="/favourite"><FavouritePage
                        goodsInFavourite={goodsInFavourite}
                        goodsInCart={goodsInCart}
                        updateDataInCart={updateDataInCart}
                        updateDataInFavourite={updateDataInFavourite}/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );

}

export default App;
